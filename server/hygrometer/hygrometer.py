#!/usr/bin/python

__author__ = 'margaret, hugh'
__copyright__ = "Copyright (c) 2014 by Atamate Limited. All Rights Reserved."


import yaml

from twisted import internet
from twisted.web import http

from atamate import app, system, objects

config_file = "config/hygrometer.yml"
hygrometer_controllers = []

"""

architect_name: Bathroom1 hygrometer
atamate_name: 1-1-HYGR1
value : 999             #Value of relative humidity
temperature: 999        #Most recent temperature reading
humidity: 999            #Most recent humidity reading
connections:
- {humidity_sensor: 1-1-HUM1}
- {temperature_sensor: 1-1-TMP1}
display_name: Bathroom1 hygrometer
uri: host/devices/1-1-HYGR1



This is what humidity device looks like
#'atamate_name': '1-2-HUM1', 'architect_name': '1-2-HUM1', 'display_name': '1-2-HUM1', 'value': 0,
---
"""
__app_id__ = "886f92bccd5fc1f9f7e7c6847567df454bafbf9d"  #mk1

class HygrometerController:

    def __init__(self, dict):  

        # dict must be supplied
        if dict is None:
            raise Exception("HygrometerController must be constructed with a dictionary")
            # dict must be the web_dict from an HYGR object
        if 'atamate_name' not in dict:
            raise Exception("HygrometerController must be constructed with an HYGR object: %s" % dict)
        atamate_name = objects.AtamateName(dict['atamate_name'])
        if atamate_name.get_code() != 'HYGR':
            raise Exception(
                "[%s] HygrometerController must be constructed with an HYGR object: %s" % (dict['atamate_name'], dict))
        self._atamate_name = atamate_name
        self._dict = dict
        self._subscriptions = []

        #get current value of temperature and humidity, and then update the dictionary and send it to the host
        #this is performed in two chained callback - first on_temp_value then on_humidity value
        #the updated object dictionary is then sent to the host
        if 'connections' in self._dict:


             def on_humidity_value(results):
                if 'value' in results:
                   #Set humidity value in our dictionary, then update host dictionary and calculate relative humidity
                   self._dict['humidity']= results['value']
                   app.get_client().call("host/set_properties", self._atamate_name.get_name(), self._dict, False)


                   self.calc_relative_humidity()

                   #We have now completed the initialisation and are ready to receive events, so subscribe for them
                   for connection in self._dict['connections']:
                        if 'temperature_sensor' in connection:
                            if connection['temperature_sensor'] not in self._subscriptions:
                                name = objects.AtamateName(connection['temperature_sensor'])
                                if name.get_code() in ['TMP']:
                                    app.get_client().subscribe(name.get_uri(), self.on_temp_event)
                                else:
                                    raise Exception( "Hygrometer configuration contains invalid connection code: %s" % (name.get_code()))
                        elif 'humidity_sensor' in connection:
                            if connection['humidity_sensor'] not in self._subscriptions:
                                name = objects.AtamateName(connection['humidity_sensor'])
                                if name.get_code() in ['HUM']:
                                    app.get_client().subscribe(name.get_uri(), self.on_humidity_event)
                                else:
                                    raise Exception( "Hygrometer configuration contains invalid connection code: %s" % (name.get_code()))
                        else:
                            raise Exception ( "Invalid connection: %s" % connection)
                else:
                   raise Exception ("No humidity value received during init")

             def on_temp_value(results):
               if 'value' in results:
                   #Set temperature value in our dictionary, and then ask for humidity value
                   self._dict['temperature']= results['value']

                   for connection in self._dict['connections']:
                      if 'humidity_sensor' in connection:
                         name = objects.AtamateName(connection['humidity_sensor'])
                         app.get_client().call("host/get_dict", name.get_name()).addCallback(on_humidity_value)
               else:
                    raise Exception ("No temperature value received during init")

             for connection in self._dict['connections']:
                if 'temperature_sensor' in connection:
                    name = objects.AtamateName(connection['temperature_sensor'])
                    app.get_client().call("host/get_dict", name.get_name()).addCallback(on_temp_value)

    def calc_relative_humidity(self):
        #Calculate relative humidity
        self._dict['value'] =(((self._dict['humidity'])/1024)-0.16)*(1.0546-0.00216*(self._dict['temperature']))/0.0062
        #Publish relative humidity
        app.get_client().call("host/set_value", "1-1-HYGR1", self._dict['value'], True)


    #MK Not tested - what do the topic uri and the event look like?
    def on_humidity_event(self, topic_uri, event):
        print "humidity event: %s" % event
        if event['value']:
            print "humidity changed to %f" % event['value']
            self._dict['humidity']= event['value']
            self.calc_relative_humidity()


    #MK Not tested- what do the topic uri and the event look like?
    def on_temp_event(self, topic_uri, event):
        print "temp event: %s" % event
        if event['value']:
            print "temperature changed to %f" % event['value']
            self._dict['temperature']= event['value']
            self.calc_relative_humidity()

    #used for unit testing
    def web_dict(self, properties=None):
        return self._dict

#End of  HygrometerController Methods


#    Hygrometer rest request - only here because it is required by app.connect_to_host
class HygrometerRestRequest(http.Request):
    def __init__(self, dict):  
        print "In HygrometerRestRequest"


def import_controllers(controllers):
    # open the config file and read the properties for the Hygrometer controllers
    try:
        with open(config_file, "r") as fin:
            yaml_string = fin.read()
            dicts = yaml.load_all(yaml_string)
            for dict in dicts:
                if dict['atamate_name'] in controllers:
                    controllers[dict['atamate_name']].update(dict)
                    hygrometer_controllers.append(HygrometerController(controllers[dict['atamate_name']]))
                else:
                    # Hygrometer object does not exist. The host must create it
                   def on_create_hygr(result):
                        controllers[dict['atamate_name']] = dict
                        hygrometer_controllers.append(HygrometerController(controllers[dict['atamate_name']]))
                        print(" Exiting on_create_hygr")
                   print("Adding callback to on_create_hygr")
                   app.get_client().call("host/create_object", dict).addCallback(on_create_hygr)

    except IOError:
        pass


#Used for unit testing
def web_dict():
    dict = []
    for controller in hygrometer_controllers:
        dict.append(controller.web_dict())
    return dict

#
def on_register_object_code(result):
    # import controllers
    app.get_client().call("host/get_device_list", "HYGR", {"include_docs": True}).addCallback(
        import_controllers)
    print("Exiting on_register_object_code")

def on_connect(client):
    # register in the app process space
    objects.register_object_code("HYGR", [])
    # register in the host process state
    app.get_client().call("host/register_object_code", "HYGR", ['humidity', 'temperature']).addCallback(
        on_register_object_code)

if __name__ == '__main__':
    config_dict = system.load_system_config()
    app.connect_to_host(
        "ws://%s:%s" % (config_dict['wamp_service_ip_address'], config_dict['wamp_service_port_number']), "hygrometer",
        HygrometerRestRequest, on_session_open=on_connect)
    internet.reactor.run()

