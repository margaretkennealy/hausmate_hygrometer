__author__ = 'hugh,margaret'
__copyright__ = "Copyright (c) 2014 by Atamate Limited. All Rights Reserved."

"""Unit test for hygrometer.py"""

from StringIO import StringIO
import unittest

from twisted import internet
from mock import MagicMock, Mock
from twisted.web import http
import sys
import __builtin__
import time
from atamate import app, objects
from hygrometer import hygrometer


class HygrometerControllerTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        objects.object_codes['HYGR'] = []
#        encoder.save_controllers = MagicMock()

    def test_init(self):
        dict = {
            'atamate_name': '1-1-HYGR1',
            'architect_name': 'Bathroom hygro',
            'display_name': 'Bathroom1 hygrometer',
            'value': 800,
            'temperature': 700,
            'humidity': 600,
            'connections': [
                {'humidity_sensor': '1-1-HUM1'},
                {'temperature_sensor': '1-1-TMP1'}
            ]
        }

        client = Mock()
        class MockCall:
            def addCallback(self, callback):
                callback({'atamate_name': '1-1-HUM1', 'architect_name': '1-1-HUM1', 'display_name': '1-1-HUM1', 'value': 23, 'uri': 'host/devices/1-1-HUM1'})
        client.call = MagicMock(return_value=MockCall())
        app.get_client = MagicMock(return_value=client)
        hygro = hygrometer.HygrometerController(dict)
        self.assertIsNotNone(hygro)
        self.assertEqual(dict, hygro._dict)
        self.assertEqual(2, client.subscribe.call_count)
        self.assertEqual(('host/devices/1-1-HUM1', hygro.on_humidity_event), client.subscribe.call_args_list[0][0])
        self.assertEqual({}, client.subscribe.call_args_list[0][1])
        self.assertEqual(('host/devices/1-1-TMP1', hygro.on_temp_event), client.subscribe.call_args_list[1][0])
        self.assertEqual({}, client.subscribe.call_args_list[1][1])
        self.assertEqual(4, client.call.call_count)
        self.assertEqual(('host/set_properties', '1-1-HYGR1', dict, False), client.call.call_args_list[2][0])
        self.assertEqual({}, client.call.call_args_list[0][1])


class hygrometerTest(unittest.TestCase):

    def test_import_controllers(self):

        fin = StringIO("""
atamate_name: 1-1-HYGR1
architect_name: Bathroom hygro
display_name: Bathroom1 hygrometer
value: 800
temperature: 700
humidity: 600
connections:
- {humidity_sensor: 1-1-HUM1}
- {temperature_sensor: 1-1-TMP1}
""")

        controlls = [
            {
            'atamate_name': '1-1-HYGR1',
            'architect_name': 'Bathroom hygro',
            'display_name': 'Bathroom1 hygrometer',
            'value': 800,
            'temperature': 700,
            'humidity': 600,
            'connections': [
                {'humidity_sensor': '1-1-HUM1'},
                {'temperature_sensor': '1-1-TMP1'}
            ]
            }
        ]

        config = {
                '1-1-HYGR1': {
                'atamate_name': '1-1-HYGR1',
                'architect_name': 'Bathroom hygro',
                'value': 999,
                'connections': [
                    {'humidity_sensor': '1-1-HUM1'},
                    {'temperature_sensor': '1-1-TMP1'}

                ]

            }
        }


        buffer = StringIO()
        client = Mock()
        app.get_client = MagicMock(return_value=client)
        sys.stdout = buffer
        old_open = __builtin__.open
        def mock_open(filename, flags):
            if filename == hygrometer.config_file:
                o = fin
                def mock_enter():
                    return o
                def mock_exit(type, value, traceback):
                    pass
                o.__enter__, o.__exit__ = mock_enter, mock_exit
            else:
                o = old_open(filename, flags)
            return o
        __builtin__.open = mock_open

        hygrometer.hygrometer_controllers[:] = []
        hygrometer.import_controllers(config)

        dicts = hygrometer.web_dict()

        self.assertEqual(len(dicts), len(controlls))
        for dict in dicts:
            c = filter(lambda x: x['atamate_name'] == dict['atamate_name'], controlls)
            self.assertEqual(1, len(c))
            self.assertEqual(c[0], dict)
        self.assertEqual('', buffer.getvalue())
        buffer.close()

        #clear up statics, globals and mocked methods
        reload(__builtin__)
        reload(hygrometer)


    def test_on_connect(self):
        client = Mock()
        app.get_client = MagicMock(return_value=client)
        hygrometer.on_connect(client)
        self.assertEqual(1, client.call.call_count)
        self.assertEqual(('host/register_object_code', 'HYGR', ['humidity', 'temperature']), client.call.call_args_list[0][0])
        self.assertEqual({}, client.call.call_args_list[0][1])

class MockRun:
    def callLater(self,time,function,*args):
        function(*args)

if __name__ == "__main__":
    unittest.main()
